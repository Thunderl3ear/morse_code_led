#include <JetsonGPIO.h>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <iostream>

int std_dit_duration = 50;

std::unordered_map<char, std::string> create_morse_table()
{
   std::unordered_map<char, std::string> morse_table;
   morse_table['A'] = ".-";
   morse_table['B'] = "-...";
   morse_table['C'] = "-.-.";
   morse_table['D'] = "-..";
   morse_table['E'] = ".";
   morse_table['F'] = "..-.";
   morse_table['G'] = "--.";
   morse_table['H'] = "....";
   morse_table['I'] = "..";
   morse_table['J'] = ".---";
   morse_table['K'] = "-.-";
   morse_table['L'] = ".-..";
   morse_table['M'] = "--";
   morse_table['N'] = "-.";
   morse_table['O'] = "---";
   morse_table['P'] = ".--.";
   morse_table['Q'] = "--.-";
   morse_table['R'] = ".-.";
   morse_table['S'] = "...";
   morse_table['T'] = "-";
   morse_table['U'] = "..-";
   morse_table['V'] = "...-";
   morse_table['W'] = ".--";
   morse_table['X'] = "-..-";
   morse_table['Y'] = "-.--";
   morse_table['Z'] = "--..-";
   morse_table[' '] = " ";
   return morse_table;
}

void blink_short(int pin_num)
{
   GPIO::output(pin_num, GPIO::HIGH);
   std::this_thread::sleep_for(std::chrono::milliseconds(std_dit_duration)); 
   GPIO::output(pin_num, GPIO::LOW);
   std::this_thread::sleep_for(std::chrono::milliseconds(std_dit_duration));
}

void blink_long(int pin_num)
{
   GPIO::output(pin_num, GPIO::HIGH);
   std::this_thread::sleep_for(std::chrono::milliseconds(3*std_dit_duration)); 
   GPIO::output(pin_num, GPIO::LOW);
   std::this_thread::sleep_for(std::chrono::milliseconds(std_dit_duration));
}

void blink_code(std::string code, int pin_num)
{
   int N = code.length();
   for(int i=0;i<N;i++)
   { 
      if(code[i]=='.')
      {
         blink_short(pin_num);
      }
      else if(code[i]=='-')
      {
         blink_long(pin_num);
      }
      else if(code[i]==' ')
      {
         std::this_thread::sleep_for(std::chrono::milliseconds(5*std_dit_duration));
      }
   }   
}

std::vector<std::string> convert_to_morse(std::string text)
{
   std::unordered_map<char, std::string> morse_table = create_morse_table();
   std::vector<std::string> morse_code;
   for(char letter:text)
   {
      morse_code.push_back(morse_table[std::toupper(letter)]);
   }
   return morse_code;
}

void transmit(std::vector<std::string> morse_code, int pin_num)
{
   for(std::string code:morse_code)
   {
      std::cout<<code<<std::endl;
      blink_code(code, pin_num);
      std::this_thread::sleep_for(std::chrono::milliseconds(2*std_dit_duration));
   }
}

int main(int argc, char* argv[]) {
   // Setup
   std::string text ="";
   if(argc>1)
   {
        for(int i=1; i<argc; i++)
        {
            text += argv[i];
        }
      int pin_number = 40;
      GPIO::setmode(GPIO::BOARD); 
      GPIO::setup(pin_number, GPIO::OUT, GPIO::LOW); 
      std::vector<std::string> morse_code = convert_to_morse(text);
      transmit(morse_code, pin_number);
      // Reset GPIO (IMPORTANT!)
      GPIO::cleanup(); // 7
   }
   else
   {
      std::cout<<"Provide an input string"<<std::endl;
   }
}

