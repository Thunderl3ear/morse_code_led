# GPIO test project - Morse code transmitter

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
* Requires `gcc`, `cmake`, `JetsonGPIO`.

## Usage
* mkdir build
* cd build
* cmake ../
* make
* ./gpio your text goes here

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
